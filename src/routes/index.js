import React ,{ Component } from "react";
import { BrowserRouter, Route,Switch } from "react-router-dom";
import Login from '../components/Login';
import Signup from '../components/Signup';
import Dashboard from '../components/Dashboard';

class Rotes extends Component {

  componentWillMount(){
    const path=window.location.pathname;
    console.log(!localStorage.usertoken);
    if(path =="/signup" && localStorage.usertoken ){
      window.location.href="/";
    }else{
      console.log(path !="/",!localStorage.usertoken );
      // if(path =="/" && !localStorage.usertoken  ){
      //   window.location.href="/";
      // }
    }
   
	
	  }
  render (){
    return (
 
    // eslint-disable-next-line no-unused-expressions
    <BrowserRouter>
    
        {localStorage.usertoken?
          <Switch><Route path='/' render={()=><Dashboard/>} exact></Route></Switch>
        :
        <Switch>
        <Route path='/' render={()=><Login/>} exact></Route>
        <Route path='/signup' render={()=><Signup/>} exact></Route>
        
        </Switch>}
    
    
        </BrowserRouter>

    
)}
 }
export default Rotes;
