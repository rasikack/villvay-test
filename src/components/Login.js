import React, { Component } from "react";

import '../css/App.css';
import '../css/Login.css';
import axios from 'axios';

import { GoogleLogin } from 'react-google-login';


class Login extends Component {
	constructor (props){
		super(props);
		this.state ={
			email:'',
			password:'',
			emailError:'',
			passwordError:'',
			loginError:''
		}
		this.login=this.login.bind(this);
	}
	setEmail = (event) =>{
		this.setState({email:event.target.value});

	}
	setPassword = (event) =>{
		this.setState({password:event.target.value});
	}

	validation =()=>{
		let emailError ="";
		
		if(!this.state.email.includes("@")){
			emailError="Invalide Email Address";
		}else{
			emailError="";
		}
		
		if(emailError){
			this.setState({emailError});
			return false;

		}else{
			this.setState({emailError});
		}
		return true;
	}

	handleSubmit=event=>{
        
		const isValid= this.validation();
		if(isValid){
			this.login();
		
		}
		
		event.preventDefault();
	}
   
	login =event=>{
	const vm=this;
		axios.post(`https://reqres.in/api/login`, { email:vm.state.email,password:vm.state.password },{ headers: { 'Content-Type': 'application/json' } })
      	.then(res => {
			  
            localStorage.setItem('usertoken',res.data.token);
			localStorage.setItem('userId',res.data.token);
			localStorage.setItem('userEmail',vm.state.email);
			 window.location.reload();
			
       
      }).catch(function (error) {
	
		if (error.response.data.error !== undefined) {
			vm.setState({loginError:error.response.data.error});
		}
		
		   
	});
		
	

	}
	loginWithGoogle =(res)=>{
		localStorage.setItem('usertoken',res.Zi.access_token);
		
		localStorage.setItem('userEmail',res.w3.U3);
		window.location.reload();
		console.log(res);
		this.setState({email:res.w3.U3})
		
	
	}
	
  render (){
	
  return (
   
    <div className="Login">
       <div className="container">


    
      <div className="container">
	<div className="d-flex justify-content-center h-100">
		<div className="card">
			<div className="card-header">
				<h3>Sign In To <span className="company-name">Villvay</span></h3> 		
				
			</div>
			<div className="card-body">
      		<form onSubmit={this.handleSubmit}>
			 {this.state.emailError? <div className="error-msg">{this.state.emailError}</div>:null}
					<div className="input-group form-group">
						<div className="input-group-prepend">
							<span className="input-group-text"><i className="fas fa-user"></i></span>
						</div>
						<input type="text" className="form-control" placeholder="Gmail" value={this.state.email} 
						 onChange={this.setEmail} />
						
					</div>
					{this.state.passwordError? <div className="error-msg">{this.state.passwordError}</div>:null}
					<div className="input-group form-group">
						<div className="input-group-prepend">
							<span className="input-group-text"><i className="fas fa-key"></i></span>
						</div>
						<input type="password" className="form-control" placeholder="password" value={this.state.password}  
						onChange={this.setPassword} type="password" />
					</div>
					<div className="row align-items-center remember">
						<input type="checkbox" />Remember Me
					</div>
					{this.state.loginError? <div className="error-msg">{this.state.loginError}</div>:null}
					<div className="form-group">
						
					<a href="#">Forgot your password?</a>
				
						<input  value="Login" className="btn float-right login_btn" disabled={!this.state.email || !this.state.password} type="submit" />
					</div>
				</form>
			</div>
			<div className="card-footer">
				<div className="d-flex justify-content-center links">
					Don't have an account?<a href="/signup">Sign Up</a>
				</div>
				<div className="d-flex justify-content-center links signInButton">
					Login With
					<GoogleLogin
						clientId="521900205081-n14d69o6msil564e4kkb6s30ddk3jep6.apps.googleusercontent.com"
						buttonText=""
						onSuccess={this.loginWithGoogle.bind(this)}
						onFailure={this.loginWithGoogle.bind(this)}
						cookiePolicy={'single_host_origin'}
					/>
				</div>
				
			</div>
		</div>
	</div>
</div>
       
      </div>
    </div>
 
  );
}
}

export default Login