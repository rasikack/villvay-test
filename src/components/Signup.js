import React, { Component } from "react";
import '../css/App.css';
import '../css/Login.css';
import axios from 'axios';

import { GoogleLogin } from 'react-google-login';
class Signup extends Component {
	constructor (props){
		super(props);
		this.state ={
			email:'',
            password:'',
            conformPassword:'',
			emailError:'',
            passwordError:'',
            conformPasswordError:'',
			SignupError:''
		}
		
	}
	
	setEmail = (event) =>{
		this.setState({email:event.target.value});

	}
	setPassword = (event) =>{
		this.setState({password:event.target.value});
    }
    setConformPassword = (event) =>{
		this.setState({conformPassword:event.target.value});
	}

	validation =()=>{
        let emailError ="";
        let conformPasswordError ="";
		
		if(!this.state.email.includes("@")){
            emailError="Invalide Email Address";
            this.setState({emailError});
            return false;
		}else{
            emailError="";
            this.setState({emailError});
           
        }

        if(this.state.conformPassword != this.state.password){
            conformPasswordError="Password Not Match";
            this.setState({conformPasswordError});
			return false;
		}else{
            conformPasswordError="";
            this.setState({conformPasswordError});
		}
		
	

		return true;
	}

	handleSubmit=event=>{
		const isValid= this.validation();
		if(isValid){
			this.Signup();
		
		}
		
		event.preventDefault();
	}

	Signup =event=>{
	const vm=this;
		axios.post(`https://reqres.in/api/register`, { email:vm.state.email,password:vm.state.password },{ headers: { 'Content-Type': 'application/json' } })
      	.then(res => {
			localStorage.setItem('usertoken',res.data.token);
		
		localStorage.setItem('userEmail',vm.state.email);
		window.location.reload();
       
      }).catch(function (error) {
		console.log(error.response);
		
		if (error.response.data.error != undefined) {
			vm.setState({SignupError:error.response.data.error});
		}
	});
		
	

	}
	loginWithGoogle =(res)=>{
		localStorage.setItem('usertoken',res.Zi.access_token);
		
		localStorage.setItem('userEmail',res.w3.U3);
		window.location.reload();
		console.log(res);
		this.setState({email:res.w3.U3})
		
	
	}
  render (){
  return (
   
    <div className="Login">
       <div className="container">


    
      <div className="container">
	<div className="d-flex justify-content-center h-100">
		<div className="card">
			<div className="card-header">
				<h3>Welcome To <span className="company-name">Villvay</span></h3> 		
				<div className="d-flex justify-content-end social_icon">
					<span><i className="fab fa-facebook-square"></i></span>
					<span><i className="fab fa-google-plus-square"></i></span>
					<span><i className="fab fa-twitter-square"></i></span>
				</div>
			</div>
			<div className="card-body">
      		<form onSubmit={this.handleSubmit}>
			 {this.state.emailError? <div className="error-msg">{this.state.emailError}</div>:null}
					<div className="input-group form-group">
						<div className="input-group-prepend">
							<span className="input-group-text"><i className="fas fa-user"></i></span>
						</div>
						<input type="text" className="form-control" placeholder="Gmail" value={this.state.email} 
						 onChange={this.setEmail} />
						
					</div>
					{this.state.passwordError? <div className="error-msg">{this.state.passwordError}</div>:null}
					<div className="input-group form-group">
						<div className="input-group-prepend">
							<span className="input-group-text"><i className="fas fa-key"></i></span>
						</div>
						<input type="password" className="form-control" placeholder="password" value={this.state.password}  
						onChange={this.setPassword} type="password" />
					</div>
                    {this.state.conformPasswordError? <div className="error-msg">{this.state.conformPasswordError}</div>:null}
					<div className="input-group form-group">
						<div className="input-group-prepend">
							<span className="input-group-text"><i className="fas fa-key"></i></span>
						</div>
						<input type="password" className="form-control" placeholder="Conform password" value={this.state.conformPassword}  
						onChange={this.setConformPassword} type="password" />
					</div>
				
					{this.state.SignupError? <div className="error-msg">{this.state.SignupError}</div>:null}
					<div className="form-group">
                        <input  value="Signup" className="btn float-right Signup_btn" 
                        disabled={!this.state.email || !this.state.password || !this.state.conformPassword} type="submit" />
					</div>
				</form>
			</div>
			<div className="card-footer">
			
			<div className="d-flex justify-content-center links signInButton">
				Login With
				<GoogleLogin
				clientId="521900205081-n14d69o6msil564e4kkb6s30ddk3jep6.apps.googleusercontent.com"
				buttonText=""
				onSuccess={this.loginWithGoogle.bind(this)}
				onFailure={this.loginWithGoogle.bind(this)}
				cookiePolicy={'single_host_origin'}
			/>
			</div>
			
		</div>
		</div>
	</div>
</div>
       
	
      </div>
    </div>
 
  );
}
}

export default Signup