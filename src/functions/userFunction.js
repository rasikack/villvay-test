import React from "react";
import axios from 'axios';
export const register = newUser =>{

    return 
   
    // eslint-disable-next-line no-unreachable
    var vm=this;
    axios.post(`https://reqres.in/api/register`, { email:newUser.email,password:newUser.password },{ headers: { 'Content-Type': 'application/json' } })
      	.then(res => {
            localStorage.setItem('usertoken',res.data.token);
            localStorage.setItem('userId',res.data.id);
			return res.data;
       
      }).catch(function (error) {
		
		vm.setState({loginError:error.response.data.error});
		   
	});
}
export const login = user =>{

    return 
    // eslint-disable-next-line no-unreachable
    var vm=this;
    axios.post(`https://reqres.in/api/login`, { email:user.email,password:user.password },{ headers: { 'Content-Type': 'application/json' } })
      	.then(res => {
              localStorage.setItem('usertoken',res.data.token);
			return res.data;
       
      }).catch(function (error) {
		
		vm.setState({loginError:error.response.data.error});
		   
	});
}

// onSubmit= event =>{
		
//     var vm=this;
//     const isValid= vm.validation();
//     if(isValid){
//         const user={
//             email:vm.state.email,
//             password:vm.state.password
//         }
//         register(user).then(res =>{
//          if(res){
//              vm.props.history.push('/')
             
//          }})
    
//     }
//     event.preventDefault();

//  }